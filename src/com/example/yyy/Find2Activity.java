package com.example.yyy;



import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class Find2Activity extends ActionBarActivity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find2);
		
		Button bt1 = (Button)findViewById(R.id.btte);
		Button bt2 = (Button)findViewById(R.id.button2);
		Button bt3 = (Button)findViewById(R.id.button3);
		Button bt4 = (Button)findViewById(R.id.button4);
		Button bt5 = (Button)findViewById(R.id.button5);
		Button bt6 = (Button)findViewById(R.id.button6);
		Button bt7 = (Button)findViewById(R.id.button7);
		Button bt8 = (Button)findViewById(R.id.button8);
		Button bt9 = (Button)findViewById(R.id.button9);
		Button bt10 = (Button)findViewById(R.id.button10);
		Button bt11 = (Button)findViewById(R.id.button11);
		Button bt12 = (Button)findViewById(R.id.button12);
		Button bt13 = (Button)findViewById(R.id.button13);
		Button bt14 = (Button)findViewById(R.id.button14);
		Button bt15 = (Button)findViewById(R.id.button15);
		Button bt16 = (Button)findViewById(R.id.button16);
		Button bt17 = (Button)findViewById(R.id.button17);
		Button bt18 = (Button)findViewById(R.id.button18);
		Button bt19 = (Button)findViewById(R.id.button19);
		Button bt20 = (Button)findViewById(R.id.button20);
		Button bt21 = (Button)findViewById(R.id.button21);
		Button bt22 = (Button)findViewById(R.id.button22);
		Button bt23 = (Button)findViewById(R.id.button23);
		Button bt24 = (Button)findViewById(R.id.button24);
		Button bt25 = (Button)findViewById(R.id.button25);
		Button bt26 = (Button)findViewById(R.id.button26);
		Button bt27 = (Button)findViewById(R.id.button27);
		Button bt28 = (Button)findViewById(R.id.button28);
		Button bt29 = (Button)findViewById(R.id.button29);
		Button bt30 = (Button)findViewById(R.id.button30);
		Button bt31 = (Button)findViewById(R.id.button31);
		Button bt32 = (Button)findViewById(R.id.button32);
		Button bt33 = (Button)findViewById(R.id.button33);
		Button bt34 = (Button)findViewById(R.id.button34);
		Button bt35 = (Button)findViewById(R.id.button35);
		Button bt36 = (Button)findViewById(R.id.button36);
		Button bt37 = (Button)findViewById(R.id.button37);
		Button bt38 = (Button)findViewById(R.id.button38);
		Button bt39 = (Button)findViewById(R.id.button39);
		Button bt40 = (Button)findViewById(R.id.button40);
		
		bt1.setOnClickListener(this);
		bt2.setOnClickListener(this);
		bt3.setOnClickListener(this);
		bt4.setOnClickListener(this);
		bt5.setOnClickListener(this);
		bt6.setOnClickListener(this);
		bt7.setOnClickListener(this);
		bt8.setOnClickListener(this);
		bt9.setOnClickListener(this);
		bt10.setOnClickListener(this);
		bt11.setOnClickListener(this);
		bt12.setOnClickListener(this);
		bt13.setOnClickListener(this);
		bt14.setOnClickListener(this);
		bt15.setOnClickListener(this);
		bt16.setOnClickListener(this);
		bt17.setOnClickListener(this);
		bt18.setOnClickListener(this);
		bt19.setOnClickListener(this);
		bt20.setOnClickListener(this);
		bt21.setOnClickListener(this);
		bt22.setOnClickListener(this);
		bt23.setOnClickListener(this);
		bt24.setOnClickListener(this);
		bt25.setOnClickListener(this);
		bt26.setOnClickListener(this);
		bt27.setOnClickListener(this);
		bt28.setOnClickListener(this);
		bt29.setOnClickListener(this);
		bt30.setOnClickListener(this);
		bt31.setOnClickListener(this);
		bt32.setOnClickListener(this);
		bt33.setOnClickListener(this);
		bt34.setOnClickListener(this);
		bt35.setOnClickListener(this);
		bt36.setOnClickListener(this);
		bt37.setOnClickListener(this);
		bt38.setOnClickListener(this);
		bt39.setOnClickListener(this);
		bt40.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.find2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	public int high(int v)
	{
		return highScore;
	}
	int count =0;
	static int highScore=-1;
	static int current;
	@Override
	public void onClick(View v) {
		int id = v.getId();
		
				switch(id)
				{
				case R.id.button17:
					Toast.makeText(this, "You found me!", Toast.LENGTH_SHORT).show();
					
					current = count-1;
					int kk = 40+current;
					if(kk<= -1)
					{ highScore = kk;}
					Intent r = new Intent(this, EndActivity.class);
					startActivity(r);					
					break;
				}
				count--;
				
				TextView kr = (TextView)findViewById(R.id.tett);
				kr.setText(""+count);
			}
	
}
